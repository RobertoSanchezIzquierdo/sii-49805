#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char*argv[])
{
	mkfifo("/tmp/TuberiaLogger", 0777);
	int pipe=open("/tmp/TuberiaLogger", O_RDONLY);

	while(1)
	{
		char cad[300];
		read(pipe, cad, sizeof(cad));

		if(strcmp(cad,"Cerrar")==0) 
		{
			printf("Salimos de tenis\n");
			break;
		}
		printf("%s\n", cad);

	}

	close(pipe);
	unlink("/tmp/TuberiaLogger");
	return 0;

}
